<?php
abstract class FileReader
{
    private $file;
    private $stream;

    public function getCode(){
      return file($this->file);
    }

    function openFile()
    {
        $this->setStream(fopen($this->file, "r"));
    }

    public function getStream()
    {
        return $this->stream;
    }

    public function setStream($stream): void
    {
        $this->stream = $stream;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($file): void
    {
        $this->file = $file;
    }

    public function closeFile(): void
    {
        fclose($this->stream);
    }
}




