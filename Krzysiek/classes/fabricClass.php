<?php


use classes\Html\Html;

class Fabric
{
    private $arrayFromFile=array();
    private $output;
    public function fabrication($input, $output)
    {

        if($input =="Html" && $output=="Json"){
          $html = new Html();
          $json = new \classes\JSON\Json();
          $this->arrayFromFile = $html->convertToArray('output/table.html');
          $this->output = $json->convertToSelf($this->arrayFromFile);

          return $this->output;
        }
    }




    public function getArray()
    {
        return $this->arrayFromFile;
    }

}