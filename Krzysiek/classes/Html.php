<?php
namespace classes\Html;
    class Html extends \FileReader implements \dataConvertInterface
    {
        public function convertToArray($file)
        { $this->setFile($file);
            $this->openFile();
            $doc = new \DOMDocument();
            $doc->loadHTMLFile($this->getFile());
            $obj = array();
            $headers = $doc->getElementsByTagName('th');

            foreach ($headers as $header) {
                $obj["headers"][] = ($header->nodeValue);
            }
            $rows = $doc->getElementsByTagName("td");
            foreach ($rows as $row) {
                $obj["rows"][] = $row->nodeValue;
            }
            $this->closeFile();
            return $obj;
        }

        function convertToSelf($arrayData)
        {
            // TODO: Implement convertToSelf() method.
        }
    }
