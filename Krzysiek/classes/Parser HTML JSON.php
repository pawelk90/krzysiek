<?php

Class HtmlToJson extends FileReader implements Parser
{
    public function parser(){

    }
    public function parse($file){
        $this->setFile($file);
        $this->openFile();

        header("Content-Type: text/plain");
        echo json_encode($this->parser(),JSON_PRETTY_PRINT);
        $this->closeFile();
    }


}