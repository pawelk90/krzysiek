<?php
Class HtmlToCSV extends FileReader implements Parser
{

    function parser()
    {
        $doc = new DOMDocument();
        $doc->loadHTMLFile($this->getFile());
        $obj=array();
        $headers = $doc->getElementsByTagName('th');
        foreach($headers as $header){
            $obj["header"][] = $header->nodeValue;
        }
        $rows = $doc->getElementsByTagName('td');
        foreach ($rows as $row){
            $obj["row"][] = $row->nodeValue;
        }
        return $obj;
    }
//skleić dwie tablice header + row
    function parse($file)
    {
            $this->setFile($file);
            $this->openFile();
            $parser = $this->parser();
            header('Content-Encoding: UTF-8');
            $headerCount = count($parser['header']);
            echo implode(', ', $parser['header']);
            echo "<br />";
            foreach($parser['row'] as $key => $row) {
                $condition = (($key + 1) % $headerCount == 0);
                $rowWithComama =  $row .",";
                echo $condition ? rtrim($rowWithComama, ',') : $rowWithComama;

                if ($condition) {
                    echo "<br />";
                }
            }
            $this->closeFile();
    }
}




?>